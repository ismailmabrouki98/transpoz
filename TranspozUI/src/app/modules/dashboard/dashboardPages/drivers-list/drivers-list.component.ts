import { Component, OnInit } from '@angular/core';
import { DriverService } from '../../../../services/services';
import { Router } from '@angular/router';
import { Driver } from '../../../../services/models/driver';
import { StrictHttpResponse } from '../../../../services/strict-http-response';

@Component({
  selector: 'app-drivers-list',
  templateUrl: './drivers-list.component.html',
  styleUrl: './drivers-list.component.scss'
})
export class DriversListComponent implements OnInit { 
    page: number = 0;
    size: number = 5;
    drivers: Driver[] = [];
    isLoading: boolean = false; 
    

      constructor(
        private  driverService : DriverService,
        private router: Router,
        
      ){

    

  }

  ngOnInit(): void {
    this.findAllDrivers();
  }

  private findAllDrivers() {
    this.isLoading = true;
    this.driverService.getAllDrivers1$Response({
        page: this.page,
        size: this.size
        
      }).subscribe({
        next: (DriverResponse: StrictHttpResponse<Driver[]>) => {
          this.drivers = DriverResponse.body;
          this.isLoading = false; 
        },
        error: (error) => {
          // Handle errors gracefully (e.g., display an error message to the user)
          console.error('Error fetching drivers:', error);
          // You can also consider emitting an error event or using a toast notification
          this.isLoading = false;
        },
        complete: () => {
          // Optional: Perform actions after the request completes (regardless of success or error)
          console.log('Drivers fetched successfully');
        }
      });

      // goToDriverDetails(driver: Driver) {
      //   // Implement navigation logic to driver details page using the router
      //   // This example assumes a route named 'driver-details' with a driver ID parameter
      //   this.router.navigate(['/driver-details', driver.id]);
      // }
  }

  // get isLastPage(): boolean{
  //     return this.page == (this.DriverResponse.len as number -1);
  // }
  gotoFirstPage(){}
  gotoPreviousPage(){}
  gotoPage(index: number){}
  gotoNextPage(){}
  gotoLastPage(){}


}
