import { Component, OnInit } from '@angular/core';
import { Client } from '../../../../services/models';
import { ClientService } from '../../../../services/services';
import { Router } from '@angular/router';
import { StrictHttpResponse } from '../../../../services/strict-http-response';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrl: './clients-list.component.scss'
})
export class ClientsListComponent implements OnInit {
  page: number = 0;
  size: number = 5;
  clients: Client[] = [];
  isLoading: boolean = false; 
  

    constructor(
      private  clientService : ClientService,
      private router: Router,
      
    ){

  

}

ngOnInit(): void {
  this.findAllClients();
}

private findAllClients() {
  this.isLoading = true;
  this.clientService.getAllClients$Response({
      page: this.page,
      size: this.size
      
    }).subscribe({
      next: (ClientResponse: StrictHttpResponse<Client[]>) => {
        this.clients = ClientResponse.body;
        this.isLoading = false; 
      },
      error: (error) => {
        // Handle errors gracefully (e.g., display an error message to the user)
        console.error('Error fetching clients:', error);
        // You can also consider emitting an error event or using a toast notification
        this.isLoading = false;
      },
      complete: () => {
        // Optional: Perform actions after the request completes (regardless of success or error)
        console.log('Clients fetched successfully');
      }
    });

    // goToDriverDetails(driver: Driver) {
    //   // Implement navigation logic to driver details page using the router
    //   // This example assumes a route named 'driver-details' with a driver ID parameter
    //   this.router.navigate(['/driver-details', driver.id]);
    // }
}

// get isLastPage(): boolean{
//     return this.page == (this.DriverResponse.len as number -1);
// }
gotoFirstPage(){}
gotoPreviousPage(){}
gotoPage(index: number){}
gotoNextPage(){}
gotoLastPage(){}


}
