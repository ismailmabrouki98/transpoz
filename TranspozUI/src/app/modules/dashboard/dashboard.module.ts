import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './dashboardPages/main/main.component';
import { MenuComponent } from './dashboardComponents/menu/menu.component';
import { DriversListComponent } from './dashboardPages/drivers-list/drivers-list.component';
import { DriverCardComponent } from './dashboardComponents/driver-card/driver-card.component';
import { ClientsListComponent } from './dashboardPages/clients-list/clients-list.component';
import { ClientCardComponent } from './dashboardComponents/client-card/client-card.component';


@NgModule({
  declarations: [
    MainComponent,
    MenuComponent,
    DriversListComponent,
    DriverCardComponent,
    ClientsListComponent,
    ClientCardComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
