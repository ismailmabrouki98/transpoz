import { Component, Input } from '@angular/core';
import { Client } from '../../../../services/models';

@Component({
  selector: 'app-client-card',
  templateUrl: './client-card.component.html',
  styleUrl: './client-card.component.scss'
})
export class ClientCardComponent {
  onShowDetails() {
    throw new Error('Method not implemented.');
    }
      private _clientCover: String | undefined;
      private _client!: Client;
      private _address : String ="";
   
  
    
    
      get client(){
        return this._client;
      }
    
      get address(){
        if (this._client.address?.streetAddress){
        return this._client.address?.streetAddress;
        }else return "No Address";
      }
    
   
      @Input()
      set client(value: Client){
        this._client = value;
      }
      get clientCover(){
        return 'https://st3.depositphotos.com/6672868/13701/v/950/depositphotos_137014128-stock-illustration-user-profile-icon.jpg';
      }
    
      // onShowDetails(): void{
    
      // }
    
}
    
