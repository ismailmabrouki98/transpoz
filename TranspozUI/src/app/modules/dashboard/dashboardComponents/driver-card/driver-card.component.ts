import { Component, Input } from '@angular/core';
import { Driver } from '../../../../services/models';

@Component({
  selector: 'app-driver-card',
  templateUrl: './driver-card.component.html',
  styleUrl: './driver-card.component.scss'
})
export class DriverCardComponent {
onShowDetails() {
throw new Error('Method not implemented.');
}
  private _driverCover: String | undefined;
  private _driver!: Driver;
  private _address : String ="";
  private _vehicleModel:  String ="";
  private _available: String ="";
  get available(){
    if (this._driver.available){
      return "Available"
    }else return "Not available";
  }

  get driver(){
    return this._driver;
  }

  get address(){
    if (this._driver.currentLocation?.address){
    return this._driver.currentLocation?.address;
    }else return "No Address";
  }

  get vehicleModel (){
      return this._driver.vehicle?.model;
  }

  @Input()
  set driver(value: Driver){
    this._driver = value;
  }
  get driverCover(){
    return 'https://st3.depositphotos.com/6672868/13701/v/950/depositphotos_137014128-stock-illustration-user-profile-icon.jpg';
  }

  // onShowDetails(): void{

  // }

}
