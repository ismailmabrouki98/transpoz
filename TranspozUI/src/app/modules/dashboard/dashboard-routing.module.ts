import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './dashboardPages/main/main.component';
import { DriversListComponent } from './dashboardPages/drivers-list/drivers-list.component';
import { ClientsListComponent } from './dashboardPages/clients-list/clients-list.component';
import { authGuard } from '../../services/guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [authGuard],
    children: [
      {
        path: 'drivers-list',
        component: DriversListComponent,
        canActivate: [authGuard]

      },
      {
        path: 'clients-list',
        component: ClientsListComponent,
        canActivate: [authGuard]

      }
    ]
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
